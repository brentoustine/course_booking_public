let urlValues = new URLSearchParams(window.location.search)


let id = urlValues.get('courseId')



let token = localStorage.getItem('token')


let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")

fetch(`https://safe-depths-18663.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	

name.innerHTML = convertedData.name,
price.innerHTML = convertedData.price,
desc.innerHTML = convertedData.description
enroll.innerHTML = `<a id="enrollButton" class="btn btn-info text-white btn-block">Enroll</a>`


document.querySelector("#enrollButton").addEventListener("click", () => {

	 fetch('https://safe-depths-18663.herokuapp.com/api/users/details', {
	     method:'GET',
	     headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	     }
       }).then(res => res.json()).then(jsonData => {

           let userCourses = jsonData.enrollments.map(courses => {

    	return courses.courseId

         })
          if (userCourses.includes(id)) {

          	Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'You are already enrolled in this course.',
			  footer: '<a href="./courses.html">Select other courses</a>'
			})

          } else {

         fetch('https://safe-depths-18663.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`//we have to get the actual value of the token variable
			},
			body: JSON.stringify({
				courseId: id 
			})
		})
		.then(res => {
			
			return res.json()
		}).then(convertedResponse => {
          if (convertedResponse === true) {
            
          	Swal.fire({
			  icon: 'success',
			  title: 'congratulations!',
			  text: 'Successfully Enrolled!',
			  footer: '<a href="./profile.html">Go to my profile page</a>'
			})
          }
			
         
          })

          }
	
		})
	})
})