let token = localStorage.getItem('token');
let profile = document.querySelector('#profileContainer')
const isAdmin = localStorage.getItem("isAdmin")

if (isAdmin === 'false' || !isAdmin) {
	fetch('https://safe-depths-18663.herokuapp.com/api/users/details', {
	method:'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {
	if (token === 0) {
		 window.location.replace('./login.html')
	} else {
	console.log(jsonData)
	profile.innerHTML = `
	 <div class="col-lg-12">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
              <div class="pic"><img src="assets/img/team/team-1.jpg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>${jsonData.firstName} ${jsonData.lastName}</h4>
                <span>Email: ${jsonData.email}</span>
                 <span>Mobile Number: ${jsonData.mobileNo}</span>
                  <table class="table">
            <tr>
              <th>Course Name</th>
              <th>Description</th>
              <th>Enrolled on</th>
              <th>Status</th>
              <tbody id="courseBody"></tbody>
            </tr>
          </table>
                <p>Social Links</p>
                <div class="social">
                   <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>
	`
  for (let i=0; i<jsonData.enrollments.length; i++) {
    fetch(`https://safe-depths-18663.herokuapp.com/api/courses/${jsonData.enrollments[i].courseId}`,{
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
    .then(res => res.json())
    .then(courseData => {
      document.getElementById('courseBody').innerHTML +=
      `
        <tr>
          <td>${courseData.name}</td>
          <td>${courseData.description}</td>
          <td> ${jsonData.enrollments[i].enrolledOn}</td>
          <td> ${jsonData.enrollments[i].status}</td>
        </tr>
      `
         })
 }
 }

})
} else {
	window.location.replace('./courses.html')
}



