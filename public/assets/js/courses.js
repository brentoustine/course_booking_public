let container = document.querySelector('#courseContainer')
let adminControls = document.querySelector('#adminButton')
let spinner = document.querySelector('#spinner-border')

const isAdmin = localStorage.getItem("isAdmin")

if (isAdmin === 'false' || !isAdmin) {
   adminControls.innerHTML = null;

} else {
	adminControls.innerHTML = `
	<div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-info">Add Course</a></div>	
	`
    
}
 fetch ('https://safe-depths-18663.herokuapp.com/api/courses').then(res => res.json()).then(jsonData => {
     	console.log(jsonData)

     	let courseData;
     	if (jsonData.length < 1) {

            courseData = "No courses Available"
            container.innerHTML = courseData;

     	} else {
        courseData = jsonData.map(course => {

        	let cardFooter;

        	if (isAdmin === "false" || !isAdmin) {
              cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">View Courses</a>`
        	} else {
                  cardFooter =
                  `<a href="" class="btn btn-info text-white btn-block">Edit Course</a> <a href="" class="btn btn-danger text-white btn-block">Delete Course</a>
                  ` 
        	}

           return(
           	 `
          	<div class="col-md-6 my-3">
          		<div class="card">
          			<div class="card-body">
          				<h3 class="card-title">${course.name}</h3>
          				<p class="card-text text-left">Price: ₱${course.price}</p>
          				<p class="card-text text-left">Description:${course.description}</p>
          				<p class="card-text text-left">created on:${course.createdOn}</p>
          			</div>
          			<div class="card-footer">
          				${cardFooter}
          			</div>
          		</div>
          	</div>
          	`
           	)    
          }).join("")
        container.innerHTML =  courseData;
     	}
     })
// fetch('http://localhost:7071/api/courses', {
// 	method:'GET',
// 	headers: {
// 		'Content-Type': 'application/json',
// 	}
// }).then(res => res.json())
// .then(jsonData => {
// 	console.log(jsonData)
// 	profile.innerHTML = `
// 	<div class="col-md-12">
// 		<section class="jumbotron my-5">
// 			<h3 class="text-center">First Name: ${jsonData.firstName}</h3>
// 			<h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
// 			<h3 class="text-center">Email: ${jsonData.email}</h3>
// 			<h3 class="text-center">Mobile Number: ${jsonData.mobileNo}</h3>
// 			<table class="Table col-md-12">
// 				<tr>
// 					<th>Course id:</th>
// 					<th>Enrolled on:</th>
// 					<th>Status:</th>
// 					<tbody></tbody>
// 				</tr>
// 			</table>
// 		</section>
// 	</div>
// 	`

// })
// container.innerHTML = `<div class="col-md-6 my-3">
// 	<div class="card">
// 		<div class="card-body">
// 			<h3 class="card-title">Course Name</h3>
// 		<p class="card-text text-left">Description</p>
// 		<p class="card-text text-left">Price</p>
// 		<p class="card-text text-left">Created On</p>
// 		</div>
// 	</div>	
// </div>`