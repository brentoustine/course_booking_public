//Create our add course page
let formSubmit = document.querySelector('#createCourse')
const isAdmin = localStorage.getItem("isAdmin")

if (isAdmin === 'false' || !isAdmin) {
   Swal.fire("Admin access denied!")
  window.location.replace('./login.html')
} else {

//lets acquire an event that will applied in our form component
//create a callback function to create a response when submitting form
formSubmit.addEventListener('submit', (event) => {
 event.preventDefault(); //avoid page redirection

let name = document.querySelector("#courseName").value
let cost = document.querySelector("#price").value
let desc = document.querySelector("#desc").value

//lets create a checker to see if were able to capture the values of each input fields
console.log(name)
console.log(cost)
console.log(desc)




  if (name !== "" && cost !== "" && desc !== "") {

   fetch('https://safe-depths-18663.herokuapp.com/api/courses/course-exists', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json'
         }, 
         body: JSON.stringify({
            name: name
         })
      }).then(res => {
          return res.json() 
      }).then(convertedData => {

         if (convertedData === false) {

          fetch('https://safe-depths-18663.herokuapp.com/api/courses/create', {
  method: 'POST',
  headers: {
    'Content-Type':'application/json'
  },
  body: JSON.stringify({
       //properties of the document that the users need to fill
       name: name,
       description: desc,
       price: cost
  })
}).then(res => {
  console.log(res)
  return res.json() //converting our response to json format
}).then(info => {
  //lets create a control structure that will describe the response of the UI to the client
  if (info === true) {
     Swal.fire({
      title:'Course Added Successfully!',
      icon:'success'

     })
  } else  {
       Swal.fire('Failed to add Course.')
  }
})
         } else {
          Swal.fire('Course Already Exists!')
         }
      })
  //send request to the backend project to process data


} else {
  Swal.fire('All Fields is Required.')
}





})
}