const loginForm = document.querySelector('#loginUser');

//the login form will be used by the client to insert his/her account
loginForm.addEventListener('submit', (event) => {
	event.preventDefault();

	let userEmail = document.querySelector('#userEmail').value
	let password = document.querySelector('#password').value


if (userEmail !== "" && password !== "") {

	fetch('https://safe-depths-18663.herokuapp.com/api/users/login', {
    method: 'POST',
    headers: {
       'Content-Type': 'application/json'
    },

    body: JSON.stringify({
        email: userEmail,
        password: password
    })

	}).then(res => {
                console.log(res)
                //console.log("hello");
                return res.json()
              }).then(data => {

                console.log(data)
                //lets create here a control structure to give out a proper response depending on the return from the backend. 



                if(data.accessToken){
                  localStorage.setItem('token', data.accessToken)
                   Swal.fire(
                      'Logged In Successfully!',
                      'Welcome!',
                      'success'
                    )

                   fetch('https://safe-depths-18663.herokuapp.com/api/users/details', {
                    // method: 'GET',
                    headers: {
                     'Authorization': `Bearer ${data.accessToken}`
                    }
                   }).then(res => {
                    return res.json()
                   }).then(user => {
                    console.log(user)
                    localStorage.setItem('id', user._id)
                    localStorage.setItem('isAdmin', user.isAdmin)
             
                    window.location.replace('./profile.html')
                   

                    
                   })


                   
                }else{
                   //inform the user that something went wrong
                    Swal.fire("Invalid username or password!")
                  }
              }
              )
  
} else {
    Swal.fire("Incomplete Fields!")
       }

	

})