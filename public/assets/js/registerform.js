// console.log("Register Test")

//targeting our form component and place it inside a new var
let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener('submit',(event)=> {
	event.preventDefault()


  //capture each values inside the input fields
let fName= document.querySelector("#rfirstName").value
// console.log(firstName)
let lastName = document.querySelector("#rlastName").value
// console.log(lastName)
let email= document.querySelector("#ruserEmail").value
// console.log( userEmail)
let mobileNo = document.querySelector("#mobileNumber").value
// console.log(mobileNo)
let password = document.querySelector("#password1").value
// console.log(password)
let verifyPassword = document.querySelector("#password2").value


// Swal.fire({
//     	icon: "success",
//     	text: "Registered Successfully"
//     })

//stretcg goal: the password should countain alpha numeric character and atleast 1 special char
// find a mobile number to accept both foreign and local formats 

//control structure to determine the next set of procedures before the user can register
if((fName  !== "" && lastName !== ""  && email !== ""  && password !== ""  && verifyPassword !== "" ) && (password === verifyPassword) && (mobileNo.length === 11))
   {  
    let passwordValidation = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
    if (!passwordValidation.test(password)) {
        Swal.fire("Password must contain alphanumeric characters and 1 special and uppercase characters.")
    } else {
         fetch('https://safe-depths-18663.herokuapp.com/api/users/email-exists', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json'
         }, 
         body: JSON.stringify({
            email: email
         })
      }).then(res => {
          return res.json() //to make it readable once the response returns to the client side. 
      }).then(convertedData => {
          //what would the response look like? 
          //lets create a control structure to determine the proper procedure depending on the response 
          if (convertedData === false) {
              //lets allow the user to register an account. 
               fetch('https://safe-depths-18663.herokuapp.com/api/users/register', {
                //we will now describe the structure of our request for register
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                //API only accepts request in a string format. 
                body: JSON.stringify({
                  firstName: fName,
                  lastName: lastName,
                  email: email,
                  mobileNo: mobileNo, 
                  password: password
                }) 
              }).then(res => {
                console.log(res)
                //console.log("hello");
                return res.json()
              }).then(data => {
                console.log(data)
                //lets create here a control structure to give out a proper response depending on the return from the backend. 
                if(data === true){
                  Swal.fire("New Account Registered Successfully!")
                   window.location.replace('./login.html')
                    
                }else{
                   //inform the user that something went wrong
                    Swal.fire("Something with wrong during processing!")
                }
              }
              )
           } else {
               //lets inform the user what went wrong 
               Swal.fire("The Email Already exist!") // you can modify the message. 
          }
      }
      )
    }


      //how are we going to integrate our email-exists method? 
      //we are going to send out a new request 
      //before you allow the user to create a new account check if the email value is still available for use. this will ensure that each user will have their unique user email 
      //upon sending this request we are instantiating a promise object, which means we should apply a method to handle the response. 
   

   	  //this block of code will run if the condition has been met
      //how can we create a new account for user using the data that he/she entered?
      //url -> describes the destination of the request. 
      //3 STATES for a promise (pending, fullfillment, rejected)
   } else {
       Swal.fire("All fields are required.")
   }
}) 
